PROJECT = "Tindog"

install: ;@echo "📦 Installing dependencies and building ${PROJECT}....."; \
	npm install && npx lerna bootstrap && npx lerna run build

docker-dev-up: ;@echo "🏗️ Launching dev docker compose ${PROJECT}....."; \
	docker compose -f docker-compose-dev.yml up --build

docker-dev-down: ;@echo "🗑️ Stopping dev docker compose ${PROJECT}....."; \
	docker compose -f docker-compose-dev.yml down

dev-gtw: ;@echo "👷 Launching dev GTW Nest Microservice ${PROJECT}....."; \
	cd ./packages/tindog-api/ && npm run dev:gtw

dev-auth: ;@echo "👷 Launching dev AUTH Nest Microservice ${PROJECT}....."; \
	cd ./packages/tindog-api/ && npm run dev:auth

dev-presence: ;@echo "👷 Launching dev PRESENCE Nest Microservice ${PROJECT}....."; \
	cd ./packages/tindog-api/ && npm run dev:presence
