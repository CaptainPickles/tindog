import { Injectable } from "@nestjs/common"
import { InjectRepository } from "@nestjs/typeorm"
import { Repository } from "typeorm"
import { TypeOrmAbstractRepository } from "./base/typeOrm.abstract.repository"
import { UserRepositoryInterface } from "../interfaces/users.repository.interface"
import { UserEntity } from "../entities/users.entity"

@Injectable()
export class UsersRepository
  extends TypeOrmAbstractRepository<UserEntity>
  implements UserRepositoryInterface
{
  constructor(
    @InjectRepository(UserEntity)
    private readonly UserRepository: Repository<UserEntity>,
  ) {
    super(UserRepository)
  }
}
