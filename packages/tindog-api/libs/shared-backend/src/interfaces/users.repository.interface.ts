import { BaseInterfaceRepository } from "../repositories/base/base.interface.repository"

import { UserEntity } from "../entities/users.entity"

export type UserRepositoryInterface = BaseInterfaceRepository<UserEntity>
