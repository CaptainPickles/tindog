import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common"
import { ClientProxy } from "@nestjs/microservices"
import { authEndpointNames } from "@tindog/iso-dto"
import { catchError, Observable, of, switchMap } from "rxjs"
import { ServicesNames } from "../shared-backend.module"

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @Inject(ServicesNames.AUTH_SERVICE)
    private readonly authService: ClientProxy,
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    if (context.getType() !== "http") {
      return false
    }

    const authHeader = context.switchToHttp().getRequest().headers[
      "authorization"
    ] as string

    if (!authHeader) return false

    const authHeaderParts = authHeader.split(" ")

    if (authHeaderParts.length !== 2) return false

    const jwt = authHeaderParts[1]

    return this.authService
      .send(
        { cmd: authEndpointNames.verifyJwt },
        {
          jwt,
        },
      )
      .pipe(
        switchMap(({ exp }) => {
          if (!exp) return of(false)

          const TOKEN_EXPIRATION_MS = exp * 1000

          const isJwtValid = Date.now() < TOKEN_EXPIRATION_MS

          return of(isJwtValid)
        }),
        catchError(() => {
          throw new UnauthorizedException()
        }),
      )
  }
}
