import { UserDto } from "@tindog/iso-dto"
import {
  Column,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm"
import { TableNames } from ".."

/**
 * Entity for the users table
 * 1 user is one dog owner
 * user can store their location and have a perimeter of people to find people with dogs
 */
@Entity(TableNames.USERS)
export class UserEntity implements UserDto {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  firstName: string

  @Column()
  lastName: string

  @Column({ unique: true })
  email: string

  @Column({ select: false })
  password: string

  // @Column({ type: "point", nullable: true }) // todo location with point column postgresql
  // location: string;
}
