import { Module } from "@nestjs/common"
import { ConfigModule, ConfigService } from "@nestjs/config"
import { TypeOrmModule } from "@nestjs/typeorm"
import { isDevEnv } from "@tindog/iso-dto"

export const enum TableNames {
  USERS = "USERS",
}

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: "./../../.env",
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: "postgres",
        url: isDevEnv()
          ? `postgresql://${configService.get(
              "POSTGRES_USER",
            )}:${configService.get(
              "POSTGRES_PASSWORD",
            )}@localhost:5432/${configService.get("POSTGRES_DB")}`
          : configService.get("POSTGRES_URI"),
        autoLoadEntities: true,
        synchronize: true, // should not be used in production can loss data
      }),
      inject: [ConfigService],
    }),
  ],
})
export class PostgresModule {}
