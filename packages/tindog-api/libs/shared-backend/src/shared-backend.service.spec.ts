import { Test, TestingModule } from "@nestjs/testing"
import { SharedBackendService } from "./shared-backend.service"

describe("SharedBackendService", () => {
  let service: SharedBackendService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SharedBackendService],
    }).compile()

    service = module.get<SharedBackendService>(SharedBackendService)
  })

  it("should be defined", () => {
    expect(service).toBeDefined()
  })
})
