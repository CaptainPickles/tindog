import { Injectable } from "@nestjs/common"
import { ConfigService } from "@nestjs/config"
import { RmqContext, RmqOptions, Transport } from "@nestjs/microservices"
import { isDevEnv } from "@tindog/iso-dto"

@Injectable()
export class SharedBackendService {
  constructor(private readonly configService: ConfigService) {}

  getRmqOptions(queue: string): RmqOptions {
    const RMQ_USER = this.configService.get("RABBITMQ_USER")
    const RMQ_PASSWORD = this.configService.get("RABBITMQ_PASS")
    const RMQ_HOST = this.configService.get("RABBITMQ_HOST")
    return {
      transport: Transport.RMQ,
      options: {
        urls: [
          isDevEnv()
            ? `amqp://${RMQ_USER}:${RMQ_PASSWORD}@localhost:5672`
            : `amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_HOST}`,
        ],
        noAck: false,
        queue: queue,
        queueOptions: {
          durable: true, // data wont be lost during restart
        },
      },
    }
  }

  acknowledgeMessage(context: RmqContext) {
    const channel = context.getChannelRef()
    const message = context.getMessage()
    channel.ack(message)
  }
}
