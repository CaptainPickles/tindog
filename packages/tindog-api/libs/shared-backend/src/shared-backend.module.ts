import { DynamicModule } from "@nestjs/common"
import { Module } from "@nestjs/common"
import { ConfigModule, ConfigService } from "@nestjs/config"
import { ClientProxyFactory, Transport } from "@nestjs/microservices"

import { isDevEnv } from "@tindog/iso-dto"
import { SharedBackendService } from "./shared-backend.service"

export const enum ServicesNames {
  AUTH_SERVICE = "AUTH_SERVICE",
  PRESENCE_SERVICE = "PRESENCE_SERVICE",
}

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: "./../../.env",
    }),
  ],
  providers: [SharedBackendService],
  exports: [SharedBackendService],
})
export class SharedBackendModule {
  static registerRmq(service: ServicesNames, queue: string): DynamicModule {
    const providers = [
      {
        provide: service,
        useFactory: (configService: ConfigService) => {
          const RMQ_USER = configService.get("RABBITMQ_USER")
          const RMQ_PASSWORD = configService.get("RABBITMQ_PASS")
          const RMQ_HOST = configService.get("RABBITMQ_HOST")
          return ClientProxyFactory.create({
            transport: Transport.RMQ,
            options: {
              urls: [
                isDevEnv()
                  ? `amqp://${RMQ_USER}:${RMQ_PASSWORD}@localhost:5672`
                  : `amqp://${RMQ_USER}:${RMQ_PASSWORD}@${RMQ_HOST}`,
              ],
              queue: queue,
              queueOptions: {
                durable: true, // data wont be lost during restart
              },
            },
          })
        },
        inject: [ConfigService],
      },
    ]

    return {
      module: SharedBackendModule,
      providers,
      exports: providers,
    }
  }
}
