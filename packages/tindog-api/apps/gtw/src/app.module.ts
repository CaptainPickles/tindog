import { Module } from "@nestjs/common"
import { ServicesNames, SharedBackendModule } from "@tindog/shared-backend"
import { AppService } from "./app.service"
import { GtwAuthController } from "./controllers/gtw.auth.controller"
import { GtwPresenceController } from "./controllers/gtw.presence.controller"

@Module({
  imports: [
    SharedBackendModule.registerRmq(
      ServicesNames.AUTH_SERVICE,
      process.env.RABBITMQ_AUTH_QUEUE,
    ),
    SharedBackendModule.registerRmq(
      ServicesNames.PRESENCE_SERVICE,
      process.env.RABBITMQ_PRESENCE_QUEUE,
    ),
  ],
  controllers: [GtwPresenceController, GtwAuthController],
  providers: [AppService],
})
export class AppModule {}
