import { Body, Controller, Get, Inject, Post } from "@nestjs/common"
import { ClientProxy } from "@nestjs/microservices"
import { authEndpointNames } from "@tindog/iso-dto"
import { ServicesNames } from "@tindog/shared-backend"

@Controller("auth")
export class GtwAuthController {
  constructor(
    @Inject(ServicesNames.AUTH_SERVICE)
    private readonly authService: ClientProxy,
  ) {}

  @Get(authEndpointNames.getUsers)
  async getUser() {
    return this.authService.send({ cmd: authEndpointNames.getUsers }, {})
  }

  @Post(authEndpointNames.login)
  async login(
    @Body("email") email: string,
    @Body("password") password: string,
  ) {
    return this.authService.send(
      { cmd: authEndpointNames.login },
      { email, password },
    )
  }

  @Post(authEndpointNames.register)
  async register(
    @Body("firstName") firstName: string,
    @Body("lastName") lastName: string,
    @Body("email") email: string,
    @Body("password") password: string,
  ) {
    return this.authService.send(
      { cmd: authEndpointNames.register },
      {
        firstName,
        lastName,
        email,
        password,
      },
    )
  }
}
