import { Controller, Get, Inject, UseGuards } from "@nestjs/common"
import { ClientProxy } from "@nestjs/microservices"
import { presenceEndpointNames } from "@tindog/iso-dto"
import { AuthGuard, ServicesNames } from "@tindog/shared-backend"

@Controller("presence")
export class GtwPresenceController {
  constructor(
    @Inject(ServicesNames.PRESENCE_SERVICE)
    private readonly presenceService: ClientProxy,
  ) {}

  @UseGuards(AuthGuard)
  @Get(presenceEndpointNames.getPresence)
  async getPresence() {
    return this.presenceService.send(
      { cmd: presenceEndpointNames.getPresence },
      {},
    )
  }
}
