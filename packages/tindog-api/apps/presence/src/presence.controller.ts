import { Controller, Get } from "@nestjs/common"
import { Ctx, MessagePattern, RmqContext } from "@nestjs/microservices"
import { presenceEndpointNames } from "@tindog/iso-dto"
import { SharedBackendService } from "@tindog/shared-backend"
import { PresenceService } from "./presence.service"

@Controller()
export class PresenceController {
  constructor(
    private readonly presenceService: PresenceService,
    private readonly sharedService: SharedBackendService,
  ) {}

  @Get()
  getHello(): string {
    return this.presenceService.getHello()
  }

  @MessagePattern({ cmd: presenceEndpointNames.getPresence })
  async getPresence(@Ctx() context: RmqContext) {
    this.sharedService.acknowledgeMessage(context)
    return { hello: this.presenceService.getHello() }
  }
}
