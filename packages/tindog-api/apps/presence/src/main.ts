import { ConfigService } from "@nestjs/config"
import { NestFactory } from "@nestjs/core"
import { SharedBackendService } from "@tindog/shared-backend"
import { PresenceModule } from "./presence.module"

async function bootstrap() {
  const app = await NestFactory.create(PresenceModule)
  const configService = app.get(ConfigService)

  const sharedService = app.get(SharedBackendService)
  const RMQ_QUEUE = configService.get("RABBITMQ_PRESENCE_QUEUE")
  app.connectMicroservice(sharedService.getRmqOptions(RMQ_QUEUE))

  app.startAllMicroservices()
}
bootstrap()
