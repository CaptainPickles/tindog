import { Module } from "@nestjs/common"
import { ConfigModule } from "@nestjs/config"
import { SharedBackendModule } from "@tindog/shared-backend"
import { PresenceController } from "./presence.controller"
import { PresenceService } from "./presence.service"

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: "./../../.env",
    }),
    SharedBackendModule,
    // SharedBackendModule.registerRmq(ServicesNames.AUTH_SERVICE,process.env.RABBITMQ_AUTH_QUEUE)
  ],
  controllers: [PresenceController],
  providers: [PresenceService],
})
export class PresenceModule {}
