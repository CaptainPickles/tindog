import { Module } from "@nestjs/common"
import { ConfigModule, ConfigService } from "@nestjs/config"
import { JwtModule } from "@nestjs/jwt"
import { TypeOrmModule } from "@nestjs/typeorm"

import {
  SharedBackendModule,
  PostgresModule,
  UserEntity,
  UsersRepository,
  SharedBackendService,
} from "@tindog/shared-backend"
import { AuthController } from "./auth.controller"
import { AuthService } from "./auth.service"
import { JwtStrategy } from "./jwt-strategy"
import { JwtGuard } from "./jwt.guard"

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: "./../../../../../.env",
    }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get("JWT_SECRET"),
        signOptions: { expiresIn: "3600s" },
      }),
      inject: [ConfigService],
    }),
    TypeOrmModule.forFeature([UserEntity]),
    SharedBackendModule,
    PostgresModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    JwtGuard,
    JwtStrategy,
    {
      provide: "UsersRepositoryInterface",
      useClass: UsersRepository,
    },
    {
      provide: "SharedServiceInterface",
      useClass: SharedBackendService,
    },
  ],
})
export class AuthModule {}
