import {
  ConflictException,
  Inject,
  Injectable,
  UnauthorizedException,
} from "@nestjs/common"
import { JwtService } from "@nestjs/jwt"
import { InjectRepository } from "@nestjs/typeorm"
import { loginDTO, NewUserDTO, UserDto } from "@tindog/iso-dto"
import * as bcrypt from "bcrypt"
import { Repository } from "typeorm"
import { UserEntity, UserRepositoryInterface } from "@tindog/shared-backend"

@Injectable()
export class AuthService {
  constructor(
    @Inject("UsersRepositoryInterface")
    private readonly usersRepository: UserRepositoryInterface,
    private readonly jwtService: JwtService,
  ) {}

  async getUsers() {
    return this.usersRepository.findAll()
  }

  async doesPasswordMatch(
    password: string,
    hashedPassword: string,
  ): Promise<boolean> {
    return bcrypt.compare(password, hashedPassword)
  }

  async validateCredential(
    email: string,
    password: string,
  ): Promise<UserEntity | null> {
    const user = await this.findByEmail(email)
    const doesUserExist = !!user
    if (!doesUserExist) return null

    const doesPasswordMatch = await this.doesPasswordMatch(
      password,
      user.password,
    )

    if (!doesPasswordMatch) return null

    return user
  }

  async login(loginPayload: loginDTO) {
    const { email, password } = loginPayload
    const user = await this.validateCredential(email, password)
    if (!user) {
      throw new UnauthorizedException()
    }

    const jwt = await this.jwtService.signAsync({ user })

    return { token: jwt }
  }

  async verifyJwt(jwt: string): Promise<{ exp: number }> {
    if (!jwt) {
      throw new UnauthorizedException()
    }

    try {
      const { exp } = await this.jwtService.verifyAsync(jwt)
      return { exp }
    } catch (error) {
      throw new UnauthorizedException()
    }
  }

  async findByEmail(email: string): Promise<UserEntity> {
    return this.usersRepository.findOneByCondition({
      where: { email },
      select: ["id", "email", "firstName", "lastName", "password"],
    })
  }

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 12)
  }

  async register(newUser: NewUserDTO): Promise<Omit<UserDto, "password">> {
    const { email, password, firstName, lastName } = newUser
    const existingUser = await this.findByEmail(email)

    if (existingUser) {
      throw new ConflictException("An account with this email already exists")
    }

    const hashedPassword = await this.hashPassword(password)
    const userToSave: NewUserDTO = {
      email,
      password: hashedPassword,
      firstName,
      lastName,
    }
    const savedUser = await this.usersRepository.save(userToSave)
    delete savedUser.password

    return savedUser
  }
}
