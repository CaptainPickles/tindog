import { ConfigService } from "@nestjs/config"
import { NestFactory } from "@nestjs/core"
import { SharedBackendService } from "@tindog/shared-backend"
import { AuthModule } from "./auth.module"

async function bootstrap() {
  const app = await NestFactory.create(AuthModule)
  const configService = app.get(ConfigService)

  const sharedService = app.get(SharedBackendService)
  const RMQ_QUEUE = configService.get("RABBITMQ_AUTH_QUEUE")
  app.connectMicroservice(sharedService.getRmqOptions(RMQ_QUEUE))

  app.startAllMicroservices()
}
bootstrap()
