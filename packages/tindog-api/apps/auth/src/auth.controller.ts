import { Controller, UseGuards } from "@nestjs/common"
import { Ctx, MessagePattern, Payload, RmqContext } from "@nestjs/microservices"
import { AuthService } from "./auth.service"
import { SharedBackendService } from "@tindog/shared-backend"
import {
  authEndpointNames,
  loginDTO,
  NewUserDTO,
  UserDto,
} from "@tindog/iso-dto"
import { JwtGuard } from "./jwt.guard"

@Controller()
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly sharedService: SharedBackendService,
  ) {}

  @MessagePattern({ cmd: authEndpointNames.getUsers })
  async getUser(@Ctx() context: RmqContext): Promise<UserDto[]> {
    this.sharedService.acknowledgeMessage(context)
    return this.authService.getUsers()
  }

  @MessagePattern({ cmd: authEndpointNames.login })
  async login(@Ctx() context: RmqContext, @Payload() loginPayload: loginDTO) {
    this.sharedService.acknowledgeMessage(context)
    return this.authService.login(loginPayload)
  }

  @MessagePattern({ cmd: authEndpointNames.verifyJwt })
  @UseGuards(JwtGuard)
  async verifyJwt(
    @Ctx() context: RmqContext,
    @Payload() payload: { jwt: string },
  ) {
    this.sharedService.acknowledgeMessage(context)
    return this.authService.verifyJwt(payload.jwt)
  }

  @MessagePattern({ cmd: authEndpointNames.register })
  async register(
    @Ctx() context: RmqContext,
    @Payload() newUser: NewUserDTO,
  ): Promise<Omit<UserDto, "password">> {
    this.sharedService.acknowledgeMessage(context)
    return this.authService.register(newUser)
  }
}
