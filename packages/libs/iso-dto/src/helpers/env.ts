const enum EnvNames {
  test = "test",
  dev = "dev",
  prod = "prod",
}

export function isDevEnv(): boolean {
  if (process.env.NODE_ENV === EnvNames.dev) {
    return true
  } else {
    return false
  }
}
