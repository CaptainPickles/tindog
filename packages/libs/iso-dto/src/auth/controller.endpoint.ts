export const enum authEndpointNames {
  getUsers = "get-users",
  register = "register",
  login = "login",
  verifyJwt = "verifyJwt",
}
