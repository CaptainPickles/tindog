export class UserDto {
  id: number
  firstName: string
  lastName: string
  email: string
  password: string
}
export class NewUserDTO {
  firstName: string
  lastName: string
  email: string
  password: string
}
export class loginDTO implements Pick<UserDto, "password" | "email"> {
  email: string
  password: string
}
