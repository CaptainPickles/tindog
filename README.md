<img src="tindogLogo.png" width="250" height="250">

# Tindog

Tindog is a web application that replicates the popular dating app, Tinder, but for dogs. This project was created by a junior software engineer to improve his skills in system design, particularly in using microservices and various technologies such as Node.js, Nest.js, Postgres, RabbitMQ, Minio, and Redis.

## Features
* User registration and login
* Creation and editing of dog profiles, including pictures and descriptions
* Swiping left or right on other dogs' profiles
* Matching with dogs who have swiped right on your profile
* Chatting with matches in real-time

## Architecture
The Tindog application is built using a microservices architecture, with each service handling a specific aspect of the application's functionality. The services communicate with each other via RabbitMQ message broker, data is stored in a PostgreSQL database, Minio is used to store dog pictures, and Redis is used as a cache for chat messages.

The following services will be implemented:

* auth-service - responsible for user authentication and authorization
* dog-service - responsible for managing dog profiles, including creation, editing, and retrieval
* match-service - responsible for matching dogs who have swiped right on each other's profiles
* chat-service - responsible for real-time messaging between matched dogs

All microservices are run in separate Docker containers, with a container orchestration tool such as Kubernetes being a potential future consideration for managing and scaling containers in production.

Tindog also includes a Gateway that communicates in HTTP/Websocket for the front end. The Gateway transfers requests to the microservices using RabbitMQ as the message broker.

## Dashboard UI

In addition, several dashboard UIs have been added to the application for easier management:

* RabbitMQ Dashboard UI: provides a web-based interface to monitor and manage the RabbitMQ message broker
* Postgres Admin Panel: provides a web-based interface to manage the PostgreSQL database
* Redis Insight: provides a web-based interface to monitor and manage the Redis cache
* Minio Dashboard: provides a web-based interface to manage the Minio storage

# Installation

To install and run Tindog on your local machine, follow these steps **(You will need Docker installed on your machine)** :

1. Clone this repository to your local machine
2. Create a .env file in the root directory and populate it with the necessary environment variables (see .env.example for a list of required variables)
3. Install the dependencies by running `npm install` in the root directory
4. Start the application by running npm run `start:dev` in the root directory. this will launch the cmd `docker compose -f docker-compose-dev.yml up --build`.

Once the application is running, you can access the various dashboard UIs at the following URLs:

* RabbitMQ Dashboard UI: http://localhost:15672
* Postgres Admin Panel: http://localhost:15432
* Redis Insight: http://localhost:8001
* Minio Dashboard: http://localhost:9001

## Future Impovements

Once all the requirements are done there are several potential improvements that could be made to Tindog, including:
* Implementing a recommendation algorithm to suggest compatible matches
* Adding more advanced chat features such as message history and typing indicators
* Enhancing the user interface to improve usability and engagement
* Adding profile a secret score
* Deploy with kubernetes

## Contributors
This project was created by Lucas Levesque, a junior software engineer interested in system design and building scalable applications.

## License
Tindog is licensed under the <ins>MIT License</ins>.